import React from "react";
import './App.css';
import { useState } from "react"
import axios from 'axios'

function App() {

  const [users, setUsers] = useState([])

  const getUsers = () => {
    axios.get('/users')
      .then(res => {
        setUsers(res.data)
       
      }).catch(err => {
        console.log(err)
      })
  }

  return (
    <div className="App">
      <h1>User app</h1>
      <div>
      <form>
        <label for="name">First name</label>
        <input type="text" id="name" name="fistname" placeholder="Your name.."></input>
        <label for="lastname">Last name</label>
        <input type="text" id="lastname" name="lastname" placeholder="Your last name.."></input>
        <label for="job">Job</label>
        <input type="text" id="job" name="job" placeholder="Your job.."></input>
        <button className="button">Create user</button>
      </form>
     </div>

      <button onClick={getUsers} className="button"> Get users </button>
      {users && users.length>0 && 
      <table>
        <tr>
          <th>Name</th>
          <th>Last name </th>
          <th>Job</th>
        </tr>
        {
          users.map(user => (
            <tr key={user.id}>
              <td>{user.first_name}</td>
              <td>{user.last_name}</td>
              <td>{user.job}</td>
            </tr>
          ))}
      </table>  }
      
      
    </div>
  );
}

export default App;
