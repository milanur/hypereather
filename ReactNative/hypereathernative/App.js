import { Button, StyleSheet,Card, Text, View, ListViewBase, FlatList } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';
import axios from 'axios'

export default function App()  {

const [data, setData] = useState([])

const getUsers = () => {
  fetch("http://192.168.0.28:8080/users")
  .then((res) => res.json())
  .then(resJson => {
    console.log('data',resJson)
    setData(resJson)
  }).catch(e => console.log(e))
}
  
    return (
      
        <View style={styles.container}>
            <View >
              <Text style={styles.title}>User App</Text>
            </View>
          <StatusBar style="auto" />
          <Button  onPress={getUsers} title='Get users'></Button>
          <FlatList 
            data={data}
            keyExtractor={(item,index) => index.toString()}
            renderItem={({item}) => {
              return (
                <View style= {styles.middle} >
                  <View>
                    <Text ><Text>First name: </Text>{item.first_name}</Text>
                    <Text><Text>Last name: </Text>{item.last_name}</Text>
                    <Text><Text>Job: </Text>{item.job}</Text>            
                </View>  
                </View>
              )
              
            }}
            />
        </View>
      
    );
    }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
    backgroundColor: "#eaeaea"
  },
  top: {
    flex: 0.3,
    backgroundColor: "grey",
    borderWidth: 5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  
  middle: {
    flex: 0.3,
    backgroundColor: "white",
    borderWidth: 5,
  },
  title: {
    marginTop: 16,
    paddingVertical: 8,
    borderWidth: 4,
    borderColor: "#20232a",
    borderRadius: 6,
    backgroundColor: "#61dafb",
    color: "#20232a",
    textAlign: "center",
    fontSize: 30,
    fontWeight: "bold"
  },
  
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  },
  
});
