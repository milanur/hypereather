const express = require('express')
const router = express.Router()
const User = require('../model/users')

//get all
router.get('/', async (req, res) => {
    try {
        const users = await User.find()
        res.json(users)
    } catch (err) {
        res.status(500).json({ message: err.message })
    }

})
//get one
router.get('/:id', getUser, (req, res) => {
    res.json(res.user)


})
// create one
router.post('/', async (req, res) => {
    const user = new User({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        job: req.body.job
    })
    try {
        const newUser = await user.save()
        res.status(201).json(newUser)
    } catch (err) {
        res.status(400).json({ message: err.message })
    }

})
//update
router.patch('/:id', getUser, async (req, res) => {
    if (req.body.first_name != null) {
        res.user.first_name = req.body.first_name
    }
    if (req.body.last_name != null) {
        res.user.last_name = req.body.last_name
    }
    if (req.body.job != null) {
        res.user.job = req.body.job
    }
    try {
        const updatedUser = await res.user.save()
        res.json(updatedUser)
    } catch (err) {
        res.status(400).json({ message: err.message })

    }
})
//delete
router.delete('/:id', getUser, async (req, res) => {
    try {
        await res.user.remove()
        res.json({ message: "Deleted user" })

    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})

async function getUser(req, res, next) {
    let user
    try {
        user = await User.findById(req.params.id)
        if (user == null) {
            return res.status(404).json({ message: "Cannot find user" })
        }
    } catch (err) {
        return res.status(500).json({ message: err.message })
    }
    res.user = user
    next()

}
module.exports = router